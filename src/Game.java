import java.awt.event.KeyEvent;

public class Game {

	public int time;
	public Controller controls;

	public Game() {
		controls = new Controller();
	}

	public void tick(boolean[] key) {
		time++;
		boolean up    = key[KeyEvent.VK_W];
		boolean down  = key[KeyEvent.VK_S];
		boolean left  = key[KeyEvent.VK_A];
		boolean right = key[KeyEvent.VK_D];
		boolean sprint = key[KeyEvent.VK_SHIFT];
		
		controls.tick(up, down, left, right, sprint);
	}

}
