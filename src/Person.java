public class Person {
	
	int color;
	int size;
	
	public Render render;
	
	public Person(int size) {
		this.size = size;
		render = new Render(size, size);
	}
}
