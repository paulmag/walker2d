public class Screen extends Render {

	public Person player1;
	
	public Screen(int width, int height) {
		super(width, height);
		
		player1 = new Person(64);
		player1.color = 0xff0000;
		
		for (int i = 0; i < 100 * 100; i++) {
			player1.render.pixels[i] = 0x00ff00;
		}
	}

	public void render(Game game) {
		for (int i = 0; i < width * height; i++) {
			pixels[i] = 0;
		}

		draw(player1.render, (int) game.controls.x - 32, (int) game.controls.y - 32);
	}
}
