public class Controller {

	public double x, y, xa, ya;
	double walkSpeed = 1;
	
	public void tick(boolean up, boolean down, boolean left, boolean right, boolean sprint) {
		if (sprint) {
			walkSpeed = 2;
		} else {
			walkSpeed = 1;
		}
		
		double xMove = 0;
		double yMove = 0;
		
		if (up) {
			yMove--;
		}
		if (down) {
			yMove++;
		}
		if (left) {
			xMove--;
		}
		if (right) {
			xMove++;
		}
		
		xa += xMove * walkSpeed;
		ya += yMove * walkSpeed;
		
		x += xa;
		y += ya;
		
		xa *= 0.1; // why this?
		ya *= 0.1;
	}
	
}
